const express = require('express');
const UserRoute = require('./routers/User')
const cookieParser = require('cookie-parser')


require('./db/conn');

const server = express();
const PORT = process.env.PORT;
const HOST = process.env.HOST;


server.use(express.json());
server.use(UserRoute);
server.use(cookieParser());


server.listen(PORT,HOST,()=>{
    console.log(`Server is up on ${HOST}:${PORT}`)
})
