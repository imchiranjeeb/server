const jwt = require('jsonwebtoken');
const User = require('../models/userSchema');

const auth = async (req, res, next) => {
    try {
        const token = req.cookies.jwtToken;
        console.log("Token is: " + token);
        const verifyTokens = jwt.verify(token,process.env.JWT_SECRET);
        const user = await User.findOne({_id:verifyTokens._id,"tokens:token":token});
        if(!user) {
            throw new Error("User not Found");
        }

        req.token = token;
        req.user = user;
        req.userID = user._id;

        next();
    } catch (error) {
        res.status(401).send(error.message);
        console.log("Error is : "+error.message);
    }
}

module.exports = auth;