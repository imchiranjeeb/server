const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const userSchema = new mongoose.Schema({
    name:{
        type:String,
        required: true
    },
    email:{
        type:String,
        required: true,
        unique: true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Not a Valid Email.');
            }
        }
    },
    phone:{
        type:Number,
        required: true
    },
    work:{
        type: String,
        required : true
    },
    password:{
        type: String,
        required: true
    },
    cpassword:{
        type: String,
        required: true
    },
    tokens: [
        {
            token:{
                type:String,
                required: true,
            }
        }
    ]
})



// Hashing the password
userSchema.pre("save", async function(next){
    const user = this
    if(user.isModified("password")){
        user.password = await bcrypt.hash(user.password,12);
        user.cpassword = await bcrypt.hash(user.cpassword,12);
    }
    next();
});

// Toeken Generation
userSchema.methods.generateAuthToken = async function(){
    const user = this
    try {
        let genToken = jwt.sign({_id:user._id.toString()},process.env.JWT_SECRET)
        user.tokens = user.tokens.concat({token:genToken})
        await user.save()
        return genToken
    } catch (error) {
        console.log(error.message);
    }
}



const User = new mongoose.model('users',userSchema);
module.exports = User;