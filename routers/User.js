const express = require('express');
const userSchema = require('../models/userSchema');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const auth = require('../middleware/auth')

require('../db/conn');

const router = express.Router();


router.get('/', (req, res) => {
    res.send("Hello Looser.")
})

router.get('/signup', (req, res) => {
    res.send("Hello Looser From Signup Page.")
});

router.post('/signup', async (req, res) => {
    const {name,email,phone,work,password,cpassword} = req.body;

    if(!name || !email || !phone || !work || !password || !cpassword){
        return res.status(422).json({error: "Please fill all the fields"})
    }

    try{
        const userExist  = await userSchema.findOne({email: email});

        if(userExist){
            return res.status(422).json({error: "User Exist"});
        }else if(password !== cpassword){
            return res.status(422).json({error: "Password are not Matching."});
        }else{
            const user = new userSchema({name,email,phone,work,password,cpassword});
            await user.save();
            res.status(201).json({message:"Registered Successfully"});
        }

    }catch(e){
        res.status(500).send(e.message)
    }
})

router.get('/login', (req, res) => {
    res.send("Hello Looser From Login Page.")
});

router.post('/login', async (req, res) => {
    try{
        const {email,password} = req.body;

        if(!email || !password){
            return res.status(400).json({error: "Invalid Credentials"})
        }

        const userLogin = await userSchema.findOne({email:email});

        if(userLogin){
            const isMatch = await bcrypt.compare(password, userLogin.password);

            const token = await userLogin.generateAuthToken();

            res.cookie("jwtToken",token,{});

            if(!isMatch){
                res.status(400).json({error: "Invalid Credentials"})
            }else{
                res.status(200).json({msg:"Logged In...."})
            }

        }else{
            res.status(404).json({error: "User not Registred"});
        }

    }catch(e){
        res.status(500).send(e.message)
    }
})

router.get('/about',auth,(req, res) => {
    try {
        res.status(200).send(req.rootUser)
    } catch (e) {
        res.status(500).send(e.message);
    }
})

router.get('/contact', (req, res) => {
    res.send("Hello Looser From Contact Page.")
})

module.exports = router;