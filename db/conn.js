const mongoose = require('mongoose');

const connLink = process.env.DB_LINK;

mongoose.connect(connLink, {
    useNewUrlParser : true,
    useCreateIndex: true,
    useUnifiedTopology : true,
}).then(() => console.log('Connectted to DB .')).catch(err => console.log(`Error ${err}`))